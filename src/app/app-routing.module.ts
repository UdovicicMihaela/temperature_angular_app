import { NgModule } from '@angular/core';
import { TableComponent } from './components/table/table.component'
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path:'table', component: TableComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
