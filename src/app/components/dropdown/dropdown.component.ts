import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
    $(document).ready(function(){
      $("#student-detail").change(function(){
          var name = $("#student-detail").val();
          console.log(name);
          $(".details").hide();
          $('.'+name).show();
      })
  })

  
  }

}
