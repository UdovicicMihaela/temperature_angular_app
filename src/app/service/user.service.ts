import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../interface/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  //getUsers(): Observable<User[]>{
    //eturn this.http.get<User[]>('http://jsonplaceholder.typicode.com/users');
 // }

  //getUser(): Observable<User>{  //<object>
    //return this.http.get<User>('http://jsonplaceholder.typicode.com/users/1');
    //get("http://localhost:8080/empl");
  //}

  getEmployee() : Observable<object>{
    return this.http.get("http://localhost:8080/"); //dodati ime gdje se nalazi taj spring
  }
}
