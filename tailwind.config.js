module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    backgroundColor: theme => ({
      ...theme('colors'),
      'graphcolor': '#141619',
    }),
  },
  variants: {
    extend: {},
  },
  plugins: [],
}

